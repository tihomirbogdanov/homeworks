package lecture05.task04;

import java.util.Scanner;

public class StringsCharsOnNewLines {

	public static void main(String[] args) {

		char ch;
		int i;
		String str;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the word : ");
		str = sc.nextLine();

		for(int i=0; i <str.length;i++){
			char ch = str.charAt(i);
		 	System.out.println(ch);
		}			
	
	}

}
