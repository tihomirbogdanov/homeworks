package lecture05.task25;

import java.util.Scanner;

public class SumOfNumbersInString {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the text :");

		String input = sc.nextLine();

		int sum = 0;

		char[] ch = input.toCharArray();

		for (int i = 0; i <= ch.length - 1; i++) {
			if (Character.isDigit(ch[i])) {
				Character ch1 = ch[i];
				String s1 = ch1.toString();
				int i1 = Integer.parseInt(s1);

				sum = sum + i1;
			}
		}
		System.out.println(sum);

	}

}
//used text "thereis4rabbitsin2cages" unknown error if in sentence have spaces ... 