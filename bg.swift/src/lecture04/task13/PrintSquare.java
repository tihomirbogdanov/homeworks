package lecture04.task13;

import java.util.Scanner;

public class PrintSquare {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int size;
		int i , j;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter a number for square size :");
		
		size = sc.nextInt();
		
		for(i =1; i<= size; i++) {
			for (j=1; j<= size ; j++) {
				if ((j==1 || j == size) || (i==1 || i == size))
					System.out.println("*");
			} else 
				System.out.println(" ");
	
			}
		
		}

}
